/**
 * @author: Adam Ford, Harrison Majerus
 */

package com.e.societysaysfrontend;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.e.societysaysfrontend.Constants.Consts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Class for new account activity
 */
public class NewAccountActivity extends AppCompatActivity {

    //Stores false if a login request has not been submitted or if the given username already exists
    private boolean isUnusedUser;

    /**
     * Creates new account activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_account);
        isUnusedUser = false;
    }

    /**
     * Back button press to get back to the login page
     * @param view
     */
    public void goToLogin(View view){
        finish();
        Intent startNewActivity = new Intent(this, LoginActivity.class);
        startActivity(startNewActivity);
    }

    /**
     * Checks valid input and creates new account on button click
     * @param view
     * @throws JSONException
     */
    public void createNewAccount(View view) throws JSONException {
        EditText username = findViewById(R.id.new_username);
        EditText password = findViewById(R.id.new_password);
        EditText email = findViewById(R.id.new_email);

        final String usernameStr = username.getText().toString();
        final String emailStr = email.getText().toString();
        final String passwordStr = password.getText().toString();
        Context context = getApplicationContext();

        if(!isUsernameValid(usernameStr, emailStr, context)){
            return;
        } else if(!isEmailValid(emailStr, context)){
            return;
        } else if(!isPasswordValid(passwordStr, context)){
            return;
        } else {
            int role = setRole(usernameStr);
            Consts.USERNAME = usernameStr;
            Consts.ROLE = role;
            pushNewAccountVolley(usernameStr, emailStr, passwordStr, role);
        }

    }

    /**
     * Checks given username to see if it is valid or has already been taken
     * @param username Username to be checked
     * @param context Context of app for displaying a Toast if invalid username
     * @return True for valid username, false otherwise
     */
    private boolean isUsernameValid(String username, String email, Context context) throws JSONException {
        //Confirms username length > 2
        if(username.length() < 3){
            Toast.makeText(context, "Your username is too short!", Toast.LENGTH_SHORT).show();
            return false;
        }

        //Checks to see if each char is valid
        for(int j = 0; j < username.length(); j++) {
            boolean validChar = false;
            for (int i = 0; i < Consts.VALID_CHARS.length; i++) {
                if(Consts.VALID_CHARS[i] == username.charAt(j)){
                    validChar = true;
                    break;
                }
            }
            if(!validChar){
                Toast.makeText(context, "Your username contains one or more invalid characters!", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        checkUsernameVolley(username, email);
        return isUnusedUser;
    }

    /**
     * Checks to see if email is valid
     * @param email email to be checked
     * @param context Current context of application
     * @return true if email is valid
     */
    public boolean isEmailValid(String email, Context context){
        boolean containsAt = false;
        boolean containsPeriod = false;
        for(int i = 0; i < email.length(); i++){
            if(email.charAt(i) == '@'){
                containsAt = true;
            } else if(email.charAt(i) == '.'){
                containsPeriod = true;
            }
        }
        if(!(containsAt && containsPeriod)) {
            if (context != null) {
                Toast.makeText(context, "Your email is invalid!", Toast.LENGTH_SHORT).show();
            }
        }
        return containsAt && containsPeriod;
    }

    /**
     * Checks to see if password is valid
     * @param password Password to be checked
     * @param context Current Context of app to display Toast if invalid
     * @return True if valid, false otherwise
     */
    private boolean isPasswordValid(String password, Context context){
        if(password.length() < 6){
            Toast.makeText(context, "Your password is too short!", Toast.LENGTH_SHORT).show();
            isUnusedUser = false;
            return false;
        }

        //Checks to see if each char is valid
        for(int j = 0; j < password.length(); j++) {
            boolean validChar = false;
            for (int i = 0; i < Consts.VALID_CHARS.length; i++) {
                if(Consts.VALID_CHARS[i] == password.charAt(j)){
                    validChar = true;
                    break;
                }
            }
            if(!validChar){
                Toast.makeText(context, "Your password contains one or more invalid characters!", Toast.LENGTH_SHORT).show();
                isUnusedUser = false;
                return false;
            }
        }
        return true;
    }

    /**
     * Sets the role of new user
     * @param username username to be checked
     * @return 0 for user, 1 for moderator, 2 for admin
     */
    public int setRole(String username){
        int role = 0;
        if(username.indexOf("moderator") >= 0){
            role = 1;
        }
        if(username.indexOf("admin") >= 0){
            role = 2;
        }
        return role;
    }

    /**
     * Checks if username already exists and if email has already been used
     * @param username Username to be checked
     */
    public void checkUsernameVolley(String username, String email) {
        RequestQueue queue = Volley.newRequestQueue(this);
        final String user = username.toLowerCase();
        final String mail = email.toLowerCase();
        JsonArrayRequest credReq = new JsonArrayRequest("http://cs309-yt-3.misc.iastate.edu:8080/myapp/login/all",
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        boolean valid = true;
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject curr = response.getJSONObject(i);
                                if(user.equals(curr.getString("name").toLowerCase()) || mail.equals(curr.getString("email").toLowerCase())){
                                    valid = false;
                                    break;
                                }
                            }
                        } catch(JSONException e){
                        }
                        if(!valid){
                            inUse();
                        } else {
                            isUnusedUser = true;
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_SHORT);
                error.printStackTrace();
            }
        });
        queue.add(credReq);
    }

    /**
     * Sends a toast to display if username of email has already
     */
    public void inUse(){
        Toast.makeText(getApplicationContext(), "Username or email is already in use!", Toast.LENGTH_SHORT);
    }

    /**
     * Push user info to create new account in database
     * @param username user's username to store
     * @param email user's email to store
     * @param password user's password to store
     */
    public void pushNewAccountVolley(String username, String email, String password, int role) {
        final Context context = getApplicationContext();
        JSONObject credJSON = new JSONObject();
        try {
            credJSON.put("name", username);
            credJSON.put("email", email);
            credJSON.put("password", password);
            credJSON.put("role", role);
        }
        catch(JSONException e){
            Toast.makeText(context, "Error populating credJSON!", Toast.LENGTH_SHORT).show();
        }
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest accountReq = new JsonObjectRequest(Request.Method.POST,
                "http://cs309-yt-3.misc.iastate.edu:8080/myapp/login/add",
                credJSON,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        goToHome();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        queue.add(accountReq);
    }

    /**
     * Called when a new account has been created and redirects to home activity
     */
    public void goToHome(){
        finish();
        Intent startNewActivity = new Intent(this, HomeActivity.class);
        startActivity(startNewActivity);
        Toast.makeText(getApplicationContext(), "Account created!", Toast.LENGTH_SHORT).show();
    }
}
