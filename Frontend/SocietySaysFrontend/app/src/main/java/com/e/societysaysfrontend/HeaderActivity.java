/**
 * @author: Adam Ford, Harrison Majerus
 */

package com.e.societysaysfrontend;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.e.societysaysfrontend.Constants.Consts;

/**
 * Class for the submit header activity
 */
public class HeaderActivity extends AppCompatActivity {
    final int GALLERY_REQUEST = 1;
    ImageView img;
    EditText editText;
    private Uri filepath;
    FirebaseStorage storage;
    StorageReference sr;

    /**
     * Creates activity and initializes Firebase connection
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_header);
        img = (ImageView) findViewById(R.id.img);
        editText = (EditText) findViewById(R.id.headerText);
        try {
            FirebaseApp.initializeApp(this);
            storage = FirebaseStorage.getInstance();
            sr = storage.getReference();
        } catch(Exception e){
            e.printStackTrace();
        }
        if(Consts.ROLE == 0){
            findViewById(R.id.toApproveHeaders).setVisibility(View.INVISIBLE);
        }
    }



    /**
     * Slides to ChatActivity
     * @param view
     */
    public void slideRightToChat(View view){
        Intent startNewActivity = new Intent(this, ChatActivity.class);
        startActivity(startNewActivity);
        overridePendingTransition(R.anim.slideright,R.anim.slideleftout);
    }
    /**
     * Slides to ViewHeaderActivity
     * @param view
     */
    public void slideLeftToViewTweets(View view){
        Intent startNewActivity = new Intent(this, ViewHeaderActivity.class);
        startActivity(startNewActivity);
        overridePendingTransition(R.anim.slideleft,R.anim.sliderightout);
    }

    /**
     * Slides to ChatActivity
     * @param view
     */
    public void slideLeftToHome(View view){
        Intent startNewActivity = new Intent(this, HomeActivity.class);
        startActivity(startNewActivity);
        overridePendingTransition(R.anim.slideleft,R.anim.sliderightout);
    }

    /**
     * Displays chosen photo if gallery was just closed
     * @param requestCode Unique number connected to an activity or request
     * @param resultCode Unique code
     * @param data Data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == GALLERY_REQUEST && resultCode == RESULT_OK && data != null ){
            filepath = data.getData();
            img.setImageURI(filepath);
        }
    }

    /**
     * Allows user to search phone for an image to upload
     * @param v
     */
    public void selectImage(View v){
        Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(gallery, GALLERY_REQUEST);
    }

    /**
     * Uploads image to firebase and sends address to server
     * @param v
     */
    public void uploadImage(View v){
        if(filepath != null ){
            final ProgressDialog pd = new ProgressDialog(this);
            pd.setTitle("Uploading");
            pd.show();

            StorageReference ref = sr.child("images/" + UUID.randomUUID().toString());
            final String currFilePath = ref.getPath();
            ref.putFile(filepath).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                    pd.dismiss();
                    getURLandPush( 1, "", currFilePath, Consts.USERNAME);
                    img.setImageResource(R.drawable.image);
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(HeaderActivity.this, "Upload Failed", Toast.LENGTH_LONG);
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                    pd.setMessage("Uploaded " + (int)progress + " %");
                }
            });
        }
    }

    /**
     * Gets firebase URL and pushes header to server
     * @param isImg 1 if header is a image
     * @param text Text if header contains text
     * @param filePath Filepath if an image
     * @param username Username of uploader
     */
    public void getURLandPush(int isImg, String text, String filePath, String username){
        StorageReference sr = FirebaseStorage.getInstance().getReference(filePath);
        sr.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                String hello = uri.toString();
                sendHeaderToServer(isImg, text, hello, username);
            }
        });
    }

    /**
     * Sends text to server and clears text box
     * @param v
     */
    public void uploadTextHeader(View v){
        String et = editText.getText().toString();
        if(!et.equals("")) {
            sendHeaderToServer(0, et, "", Consts.USERNAME);
            editText.setText("");
        }
    }

    /**
     * Sends the image or text to the server
     * @param isImg True if the upload is an image
     * @param text Filled with the user text if isImg is false
     * @param imgFile Filled with image file if isImg is true
     * @param uploader Username of the uploader
     */
    private void sendHeaderToServer( int isImg, String text, String imgFile, String uploader){
        JSONObject bodyJSON = new JSONObject();
        try {
            bodyJSON.put("isImg",isImg);
            bodyJSON.put("headerText", text);
            bodyJSON.put("imgFile", imgFile);
            bodyJSON.put("uploader", uploader);
            bodyJSON.put("response", "");
        }
        catch(JSONException e){

        }
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
                "http://cs309-yt-3.misc.iastate.edu:8080/myapp/sendHeader",
                bodyJSON,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Context context = getApplicationContext();
                        CharSequence text = "Success!";
                        int duration = Toast.LENGTH_LONG;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Context context = getApplicationContext();
                CharSequence text = "There was an error sending to server";
                int duration = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        queue.add(strReq);
    }
}
