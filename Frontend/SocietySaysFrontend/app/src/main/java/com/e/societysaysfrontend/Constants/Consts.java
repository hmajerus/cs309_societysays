/**
 * @author: Adam Ford, Harrison Majerus
 */

package com.e.societysaysfrontend.Constants;

import com.e.societysaysfrontend.R;
import com.e.societysaysfrontend.ViewHeaderActivity;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;
import com.twitter.sdk.android.tweetui.UserTimeline;

/**
 * Contains constants and global vars
 */
public final class Consts {
    /**
     * Array of valid username/password characters
     */
    public static final char[] VALID_CHARS = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '_', '$', '!'};

    /**
     * Username of the current user
     */
    public static String USERNAME = "null";

    /**
     * Boolean storing if the user has voted on the current header already
     */
    public static Boolean HASVOTED = false;

    /**
     * Role of the user.
     * -1: not logged in
     * 0: user
     * 1: moderator
     * 2: administrator
     */
    public static int ROLE = -1;



}
