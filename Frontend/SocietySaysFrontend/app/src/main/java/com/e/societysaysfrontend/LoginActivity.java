/**
 * @author: Adam Ford, Harrison Majerus
 */

package com.e.societysaysfrontend;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.e.societysaysfrontend.Constants.Consts;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.tweetui.TweetUi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * Class for login activity
 */
public class LoginActivity extends AppCompatActivity {


    /**
     * Creates login activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        final String CONSUMER_KEY = "pNPmYpwamUcjUat57umzTiXTP";
        final String CONSUMER_SECRET = "LbHER5wvyzMXRLdOnfe9uMUjXolmyDtvL8PKEunaKLKIjzlIIx";
        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(CONSUMER_KEY, CONSUMER_SECRET))
                .debug(true)
                .build();
        Twitter.initialize(config);
        new Thread(TweetUi::getInstance).start();
    }


    /**
     * Purely for testing purposes
     * @param view
     */
    public void goTowardsHome(View view){
        Consts.ROLE = 2;
        Consts.USERNAME = "testing";
        Intent startNewActivity = new Intent(this, HomeActivity.class);
        startActivity(startNewActivity);
    }

    /**
     * Calls on button click to attempt to log the user in by checking credentials
     * @param view
     */
    public void checkCredentials(View view) {
        TextView tx1 = findViewById(R.id.username_entry);
        TextView tx2 = findViewById(R.id.password_entry);
        final String username = tx1.getText().toString();
        final String password = tx2.getText().toString();
        Context context = getApplicationContext();
        if(!isUsernameValid(username, context)){
            return;
        }
        if(!isPasswordValid(password, context)){
            return;
        }
        pullVolley(username, password);
    }

    /**
     * Directs user to create account page
     * @param view
     */
    public void gotoNewAccount(View view){
        finish();
        Intent startNewActivity = new Intent(this, NewAccountActivity.class);
        startActivity(startNewActivity);
    }

    /**
     * Checks given username to see if it is valid or has already been taken
     * @param username Username to be checked
     * @param context Context of app for displaying a Toast if invalid username
     * @return True for valid username, false otherwise
     */
    public boolean isUsernameValid(String username, Context context) {
        if(username.length() == 0){
            if(context != null) {
                Toast.makeText(context, "Your username field is empty!", Toast.LENGTH_SHORT).show();
            }
            return false;
        }
        if(username.length() < 4){
            if(context != null) {
                Toast.makeText(context, "Your username is invalid!", Toast.LENGTH_SHORT).show();
            }
            return false;
        }
        return true;
    }

    /**
     * Checks to see if password is valid
     * @param password Password to be checked
     * @param context Current Context of app to display Toast if invalid
     * @return True if valid, false otherwise
     */
    public boolean isPasswordValid(String password, Context context){
        if(password.length() == 0){
            if(context != null) {
                Toast.makeText(context, "Your password field is empty!", Toast.LENGTH_SHORT).show();
            }
            return false;
        }
        if(password.length() < 6){
            if(context != null){
                Toast.makeText(context, "Your username/password combination does not exist!", Toast.LENGTH_SHORT).show();
            }
            return false;
        }
        for(int j = 0; j < password.length(); j++) {
            boolean validChar = false;
            for (int i = 0; i < Consts.VALID_CHARS.length; i++) {
                if(Consts.VALID_CHARS[i] == password.charAt(j)){
                    validChar = true;
                    break;
                }
            }
            if(!validChar){
                if(context != null) {
                    Toast.makeText(context, "Your username/password combination does not exist!!", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        }
        return true;
    }

    /**
     * Checks to see if username/password combination exists and if so logs user in and directs them to home
     * @param username Username to check
     * @param password Password to check
     */
    public void pullVolley(String username, String password) {
        RequestQueue queue = Volley.newRequestQueue(this);
        final String user = username.toLowerCase();
        final String pass = password.toLowerCase();
        JsonArrayRequest credReq = new JsonArrayRequest("http://cs309-yt-3.misc.iastate.edu:8080/myapp/login/all",
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        boolean valid = false;
                        int role = -1;
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject curr = response.getJSONObject(i);
                                if(user.equals(curr.getString("name").toLowerCase())){
                                    if(pass.equals(curr.getString("password"))){
                                       valid = true;
                                       role = curr.getInt("role");
                                       break;
                                    }
                                }
                            }
                        } catch(JSONException e){

                        }
                        if(valid){
                            Consts.USERNAME = user;
                            Consts.ROLE = role;
                            goToHome();
                        } else{
                            dneToast();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "Error!", Toast.LENGTH_SHORT);
                error.printStackTrace();
            }
        });
        queue.add(credReq);
    }

    /**
     * Displays toast in no combination exists
     */
    private void dneToast(){
        Toast.makeText(getApplicationContext(), "Username/Password combination does not exist!", Toast.LENGTH_SHORT).show();
    }

    /**
     * Called when a existing username/password combination has been entered
     */
    public void goToHome(){
        finish();
        Intent startNewActivity = new Intent(this, HomeActivity.class);
        startActivity(startNewActivity);
    }
}