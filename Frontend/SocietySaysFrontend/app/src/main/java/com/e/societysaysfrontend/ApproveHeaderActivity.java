/**
 * @author: Adam Ford, Harrison Majerus
 */

package com.e.societysaysfrontend;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.e.societysaysfrontend.Constants.Consts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Class for dis/approve header activity
 */
public class ApproveHeaderActivity extends AppCompatActivity {

    private JSONObject uploader;
    public JSONObject header;
    private JSONArray jArr;
    private int position;

    /**
     * Creates dis/approve headers activity and gets header table from server
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approve_header);
        getHeaderFromServer();
        position = -1;
        if(Consts.ROLE != 2){
            findViewById(R.id.bBanUser).setVisibility(View.INVISIBLE);
        }
    }

    /**
     * Goes to HeaderActivity
     * @param view
     */
    public void goToHeaders(View view){
        finish();
        Intent startNewActivity = new Intent(this, HeaderActivity.class);
        startActivity(startNewActivity);
    }

    /**
     * Deletes the header from the server currently being displayed and then calls nextHeader()
     */
    public void deleteHeader(View view){
        RequestQueue queue = Volley.newRequestQueue(this);
        String id = "";
        try {
            id = header.getString("id");
        } catch(JSONException e) {

        }
        final String url = "http://cs309-yt-3.misc.iastate.edu:8080/myapp/deleteHeader/" + id;
        JsonObjectRequest credReq = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        queue.add(credReq);
        nextHeader(view);
    }

    /**
     * Approves the header currently being displayed and then calls nextHeader()
     */
    public void approveHeader(View view){
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest approveReq = new JsonObjectRequest(Request.Method.POST,
                "http://cs309-yt-3.misc.iastate.edu:8080/myapp/approveHeader",
                header,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        queue.add(approveReq);
        nextHeader(view);
    }

    /**
     * Parses to the the next header that has yet to be approved or deleted
     */
    public void nextHeader(View view){
        position++;
        if(position >= jArr.length()){
            findViewById(R.id.imageHeader).setVisibility(View.GONE);
            findViewById(R.id.textHeader).setVisibility(View.GONE);
        } else{
            try {
                JSONObject tempHeader = jArr.getJSONObject(position);
                if(tempHeader.getInt("status") != 0) {
                    nextHeader(view);
                    return;
                } else {
                    header = tempHeader;
                    if(tempHeader.getInt("isImg") == 0) {
                        findViewById(R.id.imageHeader).setVisibility(View.GONE);
                        findViewById(R.id.textHeader).setVisibility(View.VISIBLE);
                        setTextView();
                    } else {
                        findViewById(R.id.textHeader).setVisibility(View.GONE);
                        findViewById(R.id.imageHeader).setVisibility(View.VISIBLE);
                        setImageView();
                    }
                    getUserObject();
                }
            } catch(JSONException e) {
                Toast.makeText(this.getApplicationContext(), "JSONException", Toast.LENGTH_SHORT);
                e.printStackTrace();
            }
        }
    }

    /**
     * Deletes the account of the user who submitted the currently displayed header
     */
    public void banUser(View view)  {
        //TODO: Eventually only admins will be able to do this
        RequestQueue queue = Volley.newRequestQueue(this);
        String user = "";
        try {
            user = header.getString("uploader");
        } catch(JSONException e) {

        }
        final String url = "http://cs309-yt-3.misc.iastate.edu:8080/myapp/login/deleteUser/" + user;
        JsonObjectRequest credReq = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        queue.add(credReq);
    }

    /**
     * Makes volley request for a header from the server
     * @return the JSONObject
     */
    public void getHeaderFromServer() {
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonArrayRequest getHeaders = new JsonArrayRequest("http://cs309-yt-3.misc.iastate.edu:8080/myapp/showHeaders",
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        jArr = response;
                        nextHeader(findViewById(android.R.id.content));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        queue.add(getHeaders);
    }

    /**
     * Gets the user object for the uploader of the current header
     */
    public void getUserObject(){
        RequestQueue queue = Volley.newRequestQueue(this);
        String user = "";
        try {
            user = header.getString("uploader");
        } catch(JSONException e) {

        }
        final String url = "http://cs309-yt-3.misc.iastate.edu:8080/myapp/login/showUsers/" + user;
        JsonObjectRequest credReq = new JsonObjectRequest(Request.Method.GET, url, null,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            uploader = response;
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });
            queue.add(credReq);
    }

    /**
     * Programmatically sets up a text view to display the header
     */
    private void setTextView() {
        final TextView tv =  findViewById(R.id.textHeader);
        try {
            tv.setText(header.getString("headerText"));
        }
        catch(JSONException e){
            e.printStackTrace();
        }
    }

    /**
     * Programatically sets an Image view and populate with image from Firebase
     */
    private void setImageView() {
        final ImageView imageview = findViewById(R.id.imageHeader);

        String path = "";
        try {
            path = header.getString("imgFile");
            //path += ".jpg";
        } catch (JSONException e) {

        }
        Glide.with(ApproveHeaderActivity.this).load(path).into(imageview);
    }
}
