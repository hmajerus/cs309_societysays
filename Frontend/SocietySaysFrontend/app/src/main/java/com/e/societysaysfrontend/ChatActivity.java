/**
 * @author: Adam Ford, Harrison Majerus
 */

package com.e.societysaysfrontend;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.drafts.Draft_6455;
import org.java_websocket.handshake.ServerHandshake;
import java.net.URI;
import java.net.URISyntaxException;
import com.e.societysaysfrontend.Constants.*;

/**
 * Class for chat activity
 */
public class ChatActivity extends AppCompatActivity {

    TextView displayChat;
    EditText chatBox;
    Button sendButton;
    private WebSocketClient cc;

    /**
     * Closes activity if the user changes activity
     * @param keyCode Key code
     * @param event Key event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            cc.close();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * Creates activity and sets up chat
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        displayChat = findViewById(R.id.displayChat);
        displayChat.setMovementMethod(new ScrollingMovementMethod());
        chatBox = findViewById(R.id.chatBox);
        sendButton = (Button)findViewById(R.id.sendButton);
        setup();
        displayChat.setText("");
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }


    /**
     * Slides to ViewHeaderActivity
     * @param view
     */
    public void slideLeftToViewTweets(View view){
        Intent startNewActivity = new Intent(this, ViewHeaderActivity.class);
        startActivity(startNewActivity);
        overridePendingTransition(R.anim.slideleft,R.anim.sliderightout);
        cc.close();
        finish();
    }

    /**
     * Slides to HeaderActivity
     * @param view
    */
    public void slideLeftToHeaders(View view){
        Intent startNewActivity = new Intent(this, HeaderActivity.class);
        startActivity(startNewActivity);
        overridePendingTransition(R.anim.slideleft,R.anim.sliderightout);
        cc.close();
        finish();
    }

    /**
     * Slides to ChatActivity
     * @param view
    */
    public void slideLeftToHome(View view){
        Intent startNewActivity = new Intent(this, HomeActivity.class);
        startActivity(startNewActivity);
        overridePendingTransition(R.anim.slideleft,R.anim.sliderightout);
        cc.close();
        finish();
    }


    /**
     * Sets up chat and logs the user into chat
     */
    public void setup(){
        Draft[] drafts = {new Draft_6455()};

        String w = "ws://cs309-yt-3.misc.iastate.edu:8080/myapp/websocket/"+Consts.USERNAME;

        try {
            Log.d("Socket:", "Trying socket");
            cc = new WebSocketClient(new URI(w), drafts[0]) {
                @Override
                public void onMessage(String message) {
                    Log.d("", "run() returned: " + message);
                    String s = displayChat.getText().toString();
                    displayChat.setText(s+"\n"+message);
                }

                @Override
                public void onOpen(ServerHandshake handshake) {
                    Log.d("OPEN", "run() returned: " + "is connecting");
                }

                @Override
                public void onClose(int code, String reason, boolean remote) {
                    Log.d("CLOSE", "onClose() returned: " + reason);
                }

                @Override
                public void onError(Exception e)
                {
                    Log.d("Exception:", e.toString());
                }
            };
        }
        catch (URISyntaxException e) {
            Log.d("Exception:", e.getMessage().toString());
            e.printStackTrace();
        }
        cc.connect();
    }

    /**
     * Sends the currently typed in text from the user
     * @param v
     */
    public void send(View v) {
        try {
            if(chatOK(chatBox.getText().toString())) {
                cc.send(chatBox.getText().toString());
            }
        }
        catch (Exception e)
        {
            Log.d("ExceptionSendMessage:", e.getMessage().toString());
        }
        chatBox.setText("");
    }

    /**
     * Searches string for bad words
     * @param message String to be searched
     * @return True if no bad words, false otherwise
     */
    public boolean chatOK(String message){
        //Can expand words later
        String[] badWords = {"idiot","stupid","dummy","ass","poop","damn","crap"};
        for(int i = 0 ; i < badWords.length ; i++){
            if(message.toLowerCase().indexOf(badWords[i]) >= 0){
                return false;
            }
        }
        return true;
    }
}




