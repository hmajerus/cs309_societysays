/**
 * @author: Adam Ford, Harrison Majerus
 */

package com.e.societysaysfrontend;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.tweetui.TimelineFilter;
import com.twitter.sdk.android.tweetui.TweetTimelineListAdapter;
import com.twitter.sdk.android.tweetui.UserTimeline;
import android.view.View;
import android.widget.ListView;
import java.util.List;


/**
 * Class for viewing tweets activity
 */
public class ViewHeaderActivity extends AppCompatActivity {

    /**
     * Creates ViewHeaderActivity and calls twitterSetup()
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_header);

        twitterSetup();
    }

    /**
     * Allows tweets to be pulled in and displayed
     */
    public void twitterSetup(){
        ListView listView = findViewById(R.id.tweets);
        final UserTimeline timeline = new UserTimeline.Builder().screenName("Society__Says").build();
        final TweetTimelineListAdapter adapter = new TweetTimelineListAdapter.Builder(ViewHeaderActivity.this)
                .setTimeline(timeline)
                .setViewStyle(R.style.tw__TweetLightWithActionsStyle)
                .build();
        listView.setAdapter(adapter);
    }
    /**
     * Slides to ChatActivity
     * @param view
     */
    public void slideLeftToHome(View view){
        Intent startNewActivity = new Intent(this, HomeActivity.class);
        startActivity(startNewActivity);
        overridePendingTransition(R.anim.slideleft,R.anim.sliderightout);
    }

    /**
     * Slides to HeaderActivity
     * @param view
     */
    public void slideRightToHeaders(View view){
        Intent startNewActivity = new Intent(this, HeaderActivity.class);
        startActivity(startNewActivity);
        overridePendingTransition(R.anim.slideright,R.anim.slideleftout);
    }

    /**
     * Slides to ChatActivity
     * @param view
     */
    public void slideRightToChat(View view){
        Intent startNewActivity = new Intent(this, ChatActivity.class);
        startActivity(startNewActivity);
        overridePendingTransition(R.anim.slideright,R.anim.slideleftout);
    }
}


