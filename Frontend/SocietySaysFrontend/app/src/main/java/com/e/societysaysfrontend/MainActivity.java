package com.e.societysaysfrontend;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.e.societysaysfrontend.Constants.Consts;
import com.twitter.sdk.android.core.DefaultLogger;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterConfig;
import com.twitter.sdk.android.tweetui.TweetUi;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final String CONSUMER_KEY = "pNPmYpwamUcjUat57umzTiXTP";
        final String CONSUMER_SECRET = "LbHER5wvyzMXRLdOnfe9uMUjXolmyDtvL8PKEunaKLKIjzlIIx";
        TwitterConfig config = new TwitterConfig.Builder(this)
                .logger(new DefaultLogger(Log.DEBUG))
                .twitterAuthConfig(new TwitterAuthConfig(CONSUMER_KEY, CONSUMER_SECRET))
                .debug(true)
                .build();
        Twitter.initialize(config);
        new Thread(TweetUi::getInstance).start();
        //if(Consts.ROLE == 0){
        //    findViewById(R.id.approvedHeadersB).setVisibility(View.INVISIBLE);
        //}
    }

    public void goToHome(View view){
        Intent startNewActivity = new Intent(this, HomeActivity.class);
        startActivity(startNewActivity);
    }

    public void goToLogin(View view){
        Intent startNewActivity = new Intent(this, LoginActivity.class);
        startActivity(startNewActivity);
    }

    public void goToHeaders(View view){
        Intent startNewActivity = new Intent(this, HeaderActivity.class);
        startActivity(startNewActivity);
    }

    public void goToApproveHeaders(View view){
        Intent startNewActivity = new Intent(this, ApproveHeaderActivity.class);
        startActivity(startNewActivity);
    }

    public void gotToChat(View view){
        Intent startNewActivity = new Intent(this, ChatActivity.class);
        startActivity(startNewActivity);
    }
}
