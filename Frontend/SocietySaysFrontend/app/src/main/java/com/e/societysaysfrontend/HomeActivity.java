/**
 * @author: Adam Ford, Harrison Majerus
 */

package com.e.societysaysfrontend;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.invoke.ConstantCallSite;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.e.societysaysfrontend.Constants.Consts;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import android.text.method.ScrollingMovementMethod;

/**
 * Class for voting activity
 */
public class HomeActivity extends AppCompatActivity {


    public TextView mTextMessage;
    public EditText text;
    public JSONObject header;
    public TextView currentTweet;
    public int currentTweetLength;
    public TextView countdown;
    public JSONObject topVoted;

    /**
     * Creates activity. initializes instance vars. Sends request to get active header and set up vote table.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        text = findViewById(R.id.charBox);
        text.setText("");
        mTextMessage = findViewById(R.id.message);
        currentTweet = (TextView)findViewById(R.id.currentTweet);
        countdown = (TextView)findViewById(R.id.countdown);

        TextView textView = (TextView) findViewById(R.id.topVotes);
        textView.setMovementMethod(new ScrollingMovementMethod());

        getHeaderFromServer();
        pullData();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

    }

    /**
     * Slides to ViewHeaderActivity
     * @param view
     */
    public void slideRightToViewTweets(View view){
        Intent startNewActivity = new Intent(this, ViewHeaderActivity.class);
        startActivity(startNewActivity);
        overridePendingTransition(R.anim.slideright,R.anim.slideleftout);
    }

    /**
     * Slides to HeaderActivity
     * @param view
     */
    public void slideRightToHeaders(View view){
        Intent startNewActivity = new Intent(this, HeaderActivity.class);
        startActivity(startNewActivity);
        overridePendingTransition(R.anim.slideright,R.anim.slideleftout);
    }

    /**
     * Slides to ChatActivity
     * @param view
     */
    public void slideRightToChat(View view){
        Intent startNewActivity = new Intent(this, ChatActivity.class);
        startActivity(startNewActivity);
        overridePendingTransition(R.anim.slideright,R.anim.slideleftout);
    }

    /**
     * Programmatically sets up a text view to display the header
     */
    private void setTextView() {
        final TextView tv = (TextView) findViewById(R.id.headerText);
        final ImageView imageview = (ImageView) findViewById(R.id.headerImage);
        imageview.setVisibility(View.GONE);
        tv.setVisibility(View.VISIBLE);
        try {
            tv.setText(header.getString("headerText"));
        }
        catch(JSONException e){
            e.printStackTrace();
        }
    }

    /**
     * Programmatically sets an Image view and populate with image from Firebase
     */
    private void setImageView() {
        final ImageView imageview = (ImageView) findViewById(R.id.headerImage);
        final TextView tv = (TextView) findViewById(R.id.headerText);
        imageview.setVisibility(View.VISIBLE);
        tv.setVisibility(View.GONE);
        String path = "";
        try {
            path = header.getString("imgFile");
        } catch (JSONException e) {

        }
        Glide.with(HomeActivity.this).load(path).into(imageview);
    }

    /**
     * Makes volley request for a header from the server
     * @return the JSONObject
     */
    public void getHeaderFromServer() {
        //Clear image and textview at the beginning
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonArrayRequest strReq = new JsonArrayRequest("http://cs309-yt-3.misc.iastate.edu:8080/myapp/showHeaders",
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {

                            header = getCurrHeader(response);
                            Toast.makeText(HomeActivity.this, "got respons", Toast.LENGTH_LONG);

                            try {
                                int check = header.getInt("isImg");
                                currentTweet.setText(header.getString("response"));
                                currentTweetLength = currentTweet.getText().toString().length();
                                if (header.getInt("isImg") == 1) {
                                    setImageView();
                                }
                                else {
                                    setTextView();
                                }
                                String time = header.getString("timepoint");
                                setupForCountdown(time);
                            }
                            catch(JSONException e){

                            }
                        }
                        catch(JSONException e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        queue.add(strReq);
    }

    /**
     * Called by a button click to submit the character to the server
     * @param view
     */
    public void sendChar(View view){
        checkThenSend(text.getText().toString(), view);
    }

    /**
     * Checks to see if the user as already voted and there is a intput.
     * @param toVote The vote
     * @param view
     * @return True if vote is valid and user has not voted
     */
    public boolean checkThenSend(String toVote, View view){
        if(Consts.HASVOTED){
            Toast.makeText(this.getApplicationContext(), "You cannot vote twice", Toast.LENGTH_SHORT).show();
            return false;
        }
        else {
            if (toVote.equals("")) {
                Toast.makeText(this, "Enter a character to vote", Toast.LENGTH_SHORT).show();
                return false;
            }
            pushVolley(view, toVote);
            text.setText("");
            return true;
        }
    }

    /**
     * Retrieves votes table from server and sets up the top votes for display
     */
    public void pullData() {
        final TextView txtView = findViewById(R.id.topVotes);
        RequestQueue queue = Volley.newRequestQueue(this);

        JsonArrayRequest strReq = new JsonArrayRequest("http://cs309-yt-3.misc.iastate.edu:8080/myapp/showVotes",
                new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                String toDisplay = "Top Voted Characters\n";
                try {
                    topVoted = response.getJSONObject(0);
                    for (int i = 0; i < response.length(); i++) {
                        JSONObject curr = response.getJSONObject(i);
                        String letter = curr.getString("letter");
                        if( letter.equals("×")){
                            letter = "Backspace";
                        }
                        if( letter.equals("~")){
                            letter = "Send";
                        }

                        if(letter.equals(" ")){
                            letter = "Space";
                        }
                        toDisplay += letter;
                        toDisplay += "      ";
                        toDisplay += curr.getString("votes");
                        toDisplay += "\n";
                    }
                }
                catch(JSONException e){

                }
                txtView.setText(toDisplay);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                txtView.setText("Error!");
                error.printStackTrace();
            }
        });

        queue.add(strReq);

    }

    /**
     * Pushes the character contained within the text box to the server to be counted
     * @param view
     * @param pushChar Character to be pushed
     */
    public void pushVolley(View view, String pushChar) {
        String charToShow = pushChar;
        if( charToShow.equals("×")){
            charToShow = "Backspace";
        }
        else if( charToShow.equals("~")){
            charToShow = "Send";
        }
        else if(charToShow.equals(" ")){
            charToShow = "Space";
        }
        else{
            charToShow = "\"" + charToShow + "\"";
        }
        final String forToast = charToShow;
        Consts.HASVOTED = true;
        final String charToPush = pushChar;
        JSONObject bodyJSON = new JSONObject();
        try {
            bodyJSON.put("id", null);
            bodyJSON.put("tID", "1");
            bodyJSON.put("letter", charToPush.toString());
            bodyJSON.put("votes", "1");
        }
        catch(JSONException e){

        }
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest strReq = new JsonObjectRequest(Request.Method.POST,
                "http://cs309-yt-3.misc.iastate.edu:8080/myapp/sendVote",
                bodyJSON,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Context context = getApplicationContext();
                        CharSequence text = "Your vote for " + forToast + " has been counted!";
                        int duration = Toast.LENGTH_LONG;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Context context = getApplicationContext();
                CharSequence text = "There was an error counting your vote";
                int duration = Toast.LENGTH_LONG;
                Consts.HASVOTED = false;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }
        };
        queue.add(strReq);
    }

    /**
     * Called when user hits backspace to increment vote on server
     * @param view
     */
    public void voteForBackspace(View view){
        if(!Consts.HASVOTED){
            pushVolley(view, "×");
        } else{
            Toast.makeText(HomeActivity.this, "You may only vote once", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Called when user hits sent tweet to increment vote on server
     * @param view
     */
    public void voteToSendTweetAsIs(View view){
        if(!Consts.HASVOTED){
            pushVolley(view, "~");
        }else{
            Toast.makeText(HomeActivity.this, "You may only vote once", Toast.LENGTH_LONG).show();
        }

    }

    /**
     * Setup countdown timer
     * @param timePoint Time point received from server
     */
    public void setupForCountdown(String timePoint){
        //Simple Date Format
        Date d = null;
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
            d = dateFormat.parse(timePoint);
        }
        catch (ParseException p ){

        }
        Date now = new Date();
        long dGetTime = d.getTime();
        long nowGetTime = now.getTime();
        long difference = dGetTime - nowGetTime;
        CountDownTimer cd = new CountDownTimer(difference, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long seconds = millisUntilFinished/1000;
                String sec = String.valueOf(seconds);
                countdown.setText(sec);
                int cursorBlink  = currentTweet.getText().toString().length();
                if( cursorBlink > currentTweetLength){
                    currentTweet.setText(currentTweet.getText().toString().substring(0, currentTweet.getText().toString().length()-1));
                }
                else{
                    currentTweet.setText(currentTweet.getText().toString() + "I");
                }
                pullData();
            }
            @Override
            public void onFinish() {
                Consts.HASVOTED = false;
                try {
                    if(topVoted != null) {
                        if (currentTweetLength >= 119 || topVoted.getString("letter").equals("~")) {
                            onNewTweet();
                        } else {
                            onNewVote();
                        }
                    }
                    else{
                        onNewVote();
                    }
                }catch(JSONException je){

                }
            }
        };
        cd.start();
    }

    /**
     * Called between votes. Displays message/progress.
     */
    public void onNewVote(){
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("New Vote");
        progress.setMessage("Refreshing...");
        progress.show();

        Runnable progressRunnable = new Runnable() {

            @Override
            public void run() {
                progress.cancel();
                refresh();
            }
        };

        Handler pdCanceller = new Handler();
        pdCanceller.postDelayed(progressRunnable, 1000);

    }

    /**
     * Called when a new tweet is going to be started. Displays message/progress and waits a period of time
     */
    public void onNewTweet(){
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("New Header");
        progress.setMessage("Tweeting tweet, finding new header, and refreshing your page");
        progress.show();

        Runnable progressRunnable = new Runnable() {

            @Override
            public void run() {
                progress.cancel();
                refresh();
            }
        };

        Handler pdCanceller = new Handler();
        pdCanceller.postDelayed(progressRunnable, 8000);
    }
    /**
     * Refreshes the current header and vote table
     */
    public void refresh(){
        getHeaderFromServer();
        pullData();
    }

    /**
     * Finds the current active header
     * @param response Header table
     * @return Active header
     * @throws JSONException
     */
    public static JSONObject getCurrHeader(JSONArray response) throws JSONException{
        for( int i = 0 ; i < response.length() ; i++ ){
            if( response.getJSONObject(i).getInt("status") == 2){
                //this may break if there are not approved headers
                return response.getJSONObject(i);
            }
        }
        return response.getJSONObject(response.length()-1);
    }
}