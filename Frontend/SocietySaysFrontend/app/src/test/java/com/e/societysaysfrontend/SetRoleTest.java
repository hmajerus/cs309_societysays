package com.e.societysaysfrontend;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SetRoleTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    NewAccountActivity newAccount;

    //Test setRole()

    /**
     * Test with typical username
     */
    @Test
    public void setRoleTest1(){
        when(newAccount.setRole("JustaUser")).thenReturn(0);
    }

    /**
     * Test with another generic username
     */
    @Test
    public void setRoleTest2(){
        when(newAccount.setRole("BigMan75")).thenReturn(0);
    }

    /**
     * Test with username containing the word moderator
     */
    @Test
    public void setRoleTest3(){
        when(newAccount.setRole("JustamoderatorUser")).thenReturn(1);
    }

    /**
     * Test with username containing the word admin
     */
    @Test
    public void setRoleTest4(){
        when(newAccount.setRole("adminSteve")).thenReturn(2);
    }

    /**
     * Test with username containing the word admin in a different index location
     */
    @Test
    public void setRoleTest5(){
        when(newAccount.setRole("12Admin34")).thenReturn(2);
    }
}
