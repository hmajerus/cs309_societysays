package com.e.societysaysfrontend;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UsernameValidityTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    LoginActivity login;

    //Test isUsernameValid()

    /**
     * Tests isUsernameValid() with generic username
     */
    @Test
    public void usernameValidity1() {
        String username = "username";
        when(login.isUsernameValid(username, null)).thenReturn(true);
    }

    /**
     * Tests LoginActivity.isUsernameValid() with an empty username field
     */
    @Test
    public void usernameValidity2() {
        String username = "";
        when(login.isUsernameValid(username, null)).thenReturn(false);

    }

    /**
     * Tests LoginActivity.isUsernameValid() with a username that is too short
     */
    @Test
    public void usernameValidity3(){
        String username = "a?+";
        when(login.isUsernameValid(username, null)).thenReturn(false);
    }

    /**
     * Tests LoginActivity.isUsernameValid() with a long username that should still work
     */
    @Test
    public void usernameValidity4(){
        String username = "Thisisareallylongusername";
        when(login.isUsernameValid(username, null)).thenReturn(true);

    }
}
