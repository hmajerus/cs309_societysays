package com.e.societysaysfrontend;

import android.content.Context;
import android.widget.EditText;

import com.e.societysaysfrontend.Constants.Consts;
import com.e.societysaysfrontend.HomeActivity;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HomeActivityTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    //ATTEMPT at Real Mockito
    @Mock
    HomeActivity home;
    //Check then send method
    @Test
    public void userHasVoted(){
        Consts.HASVOTED = true;
        when(home.checkThenSend("a", null)).thenReturn(false);
    }

    @Test
    public void noString(){
        Consts.HASVOTED = false;
        when(home.checkThenSend("", null)).thenReturn(false);
    }

    @Test
    public void ok() {
        Consts.HASVOTED = false;
        when(home.checkThenSend("a", null)).thenReturn(true);
    }

}
