package com.e.societysaysfrontend;

import android.content.Context;

import com.e.societysaysfrontend.Constants.Consts;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NewAccountActivityTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    NewAccountActivity naa;
    //Email Tests
    @Test
    public void emailInvalidNoPeriod() throws RuntimeException{
        String email = "Adam@gmail com";
        when(naa.isEmailValid(email, null)).thenReturn(false);
    }

    @Test
    public void emailInvalidNoAt(){
        String email = "Adam gmail.com";
        when(naa.isEmailValid(email, null)).thenReturn(false);
    }

    @Test
    public void emailInvalidBoth(){
        String email = "Adam gmail com";
        when(naa.isEmailValid(email, null)).thenReturn(false);
    }

    @Test
    public void emailValid(){
        String email = "Adam@gmail.com";
        when(naa.isEmailValid(email, null)).thenReturn(true);;
    }

    @Test
    public void emailValid2(){
        String email = "LaZerL0RD1738@yahoo.com";
        when(naa.isEmailValid(email, null)).thenReturn(true);
    }


}
