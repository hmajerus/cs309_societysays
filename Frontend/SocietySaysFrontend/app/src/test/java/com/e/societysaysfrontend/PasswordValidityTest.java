package com.e.societysaysfrontend;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PasswordValidityTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    LoginActivity login;

    //Test isPasswordValid()

    /**
     * Tests LoginActivity.isPasswordValid() with a generic password
     */
    @Test
    public void passwordValidity1(){
        String password = "password";
        when(login.isPasswordValid(password, null)).thenReturn(true);
    }

    /**
     * Tests LoginActivity.isPasswordValid() with a empty password field
     */
    @Test
    public void passwordValidity2(){
        String password = "";
        when(login.isPasswordValid(password, null)).thenReturn(false);
    }

    /**
     * Tests LoginActivity.isPasswordValid() with a long password
     */
    @Test
    public void passwordValidity3(){
        String password = "Thisisareallylongpasssssssssssssssssssswooooooooorrrrd";
        when(login.isPasswordValid(password, null)).thenReturn(true);
    }

    /**
     * Tests LoginActivity.isPasswordValid() with a password that is too short
     */
    @Test
    public void passwordValidity4(){
        String password = "p";
        when(login.isPasswordValid(password, null)).thenReturn(false);
    }

    /**
     * Tests LoginActivity.isPasswordValid() with a password that is too short
     */
    @Test
    public void passwordValidity5(){
        String password = "passw";
        when(login.isPasswordValid(password, null)).thenReturn(false);
    }

    /**
     * Tests LoginActivity.isPasswordValid() with a password that contains invalid characters
     */
    @Test
    public void passwordValidity6(){
        String password = "^&$/{}";
        when(login.isPasswordValid(password, null)).thenReturn(false);
    }
}
