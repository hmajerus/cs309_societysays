package com.e.societysaysfrontend;

import android.content.Context;

import com.e.societysaysfrontend.Constants.Consts;
import com.e.societysaysfrontend.HomeActivity;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChatActivityTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    ChatActivity ca;
    //ChatOK test
    @Test
    public void badChatNoCaps(){
        String message = "You're an idiot";
        when(ca.chatOK(message)).thenReturn(false);
    }

    @Test
    public void badChatCaps(){
        String message = "You're an IdiOt";
        when(ca.chatOK(message)).thenReturn(false);
    }

    @Test
    public void badChatNoCaps2(){
        String message = "You're a huge dummy";
        when(ca.chatOK(message)).thenReturn(false);
    }

    @Test
    public void badChatCaps2(){
        String message = "You're a huge DUMMY";
        when(ca.chatOK(message)).thenReturn(false);
    }

    @Test
    public void goodChat(){
        String message = "We should vote for f";
        when(ca.chatOK(message)).thenReturn(true);
    }

    @Test
    public void goodChat2(){
        String message = "That last tweet is so funny";
        when(ca.chatOK(message)).thenReturn(true);
    }
}
