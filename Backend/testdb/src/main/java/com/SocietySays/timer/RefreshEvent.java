package com.SocietySays.timer;

import com.SocietySays.headers.bean.Header;
import com.SocietySays.headers.service.HeadersService;
import com.SocietySays.repository.CurrentVoteRepository;
import com.SocietySays.repository.HeadersRepository;
import com.SocietySays.twitter.TwitterConfig;
import com.SocietySays.votes.bean.CurrentVote;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import twitter4j.TwitterException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
public class RefreshEvent {

    private CurrentVoteRepository votesRepository;

    private HeadersRepository headersRepository;

    private HeadersService headersService;

    private TwitterConfig twitterConfig;


    public RefreshEvent(CurrentVoteRepository votesRepository, HeadersRepository headersRepository, HeadersService headersService) {
        this.votesRepository = votesRepository;
        this.headersRepository = headersRepository;
        this.headersService = headersService;

    }

    /**
     * At the end of the timer (60 seconds), appends the top-voted char to the response string, clears the current_vote
     * table, sets a new timepoint, and sends the Header if it has reached its max length or if users voted to send
     */
    @Scheduled(fixedRate = 60000)
    public void refresh() {
        if (headersRepository.getByStatus(2).size() <= 0) {
            if (headersRepository.getByStatus(1).size() > 0) {
                headersRepository.setActive(headersRepository.getByStatus(1).get(0).getId());
            }
        }
        Header activeHeader = headersRepository.getByStatus(2).get(0);

        List<CurrentVote> votes = votesRepository.findAll(new Sort(Sort.Direction.DESC, "votes"));

        votesRepository.clearTable();


        if (votes.size() > 0) {

            if (votes.get(0).getLetter() != '~') {
                headersService.appendChar(votes.get(0).getLetter(), activeHeader.getId());
            }

            if (activeHeader.getResponse().length() >= 120 || votes.get(0).getLetter() == '~') {

                if (activeHeader.getIsImg() != 0) {
                    try {
                        twitterConfig.createImageTweet(activeHeader.getImgFile(),
                                activeHeader.getResponse());
                    } catch (TwitterException e) {
                        e.printStackTrace();
                    }
                } else {
                    String tweet = activeHeader.getHeaderText() + "\r\n\r\n" + activeHeader.getResponse();
                    try {
                        twitterConfig.createTweet(tweet);
                    } catch (TwitterException e) {
                        e.printStackTrace();
                    }
                }


                headersRepository.deleteById(activeHeader.getId());

                if (headersRepository.getByStatus(1).size() > 0) {
                    activeHeader = headersRepository.getByStatus(1).get(0);
                }

                headersRepository.setActive(activeHeader.getId());
            }
        }

        Date timePoint = new Date();
        timePoint.setMinutes(timePoint.getMinutes() + 1);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ");
        String formatted = dateFormat.format(timePoint);
        headersRepository.updateTimepoint(formatted, activeHeader.getId());
    }
}
