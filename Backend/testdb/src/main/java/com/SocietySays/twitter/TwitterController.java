package com.SocietySays.twitter;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import twitter4j.TwitterException;

@RestController
public class TwitterController {

    @PostMapping("/posttweet/{tweet}")
    public String postTweet(@PathVariable("tweet") String tweet) {
        try {
            return TwitterConfig.createTweet(tweet);
        } catch (TwitterException e) {
            return "failed";
        }
    }

    @PostMapping("/postimage/{tweet}")
    public String postImageTweet(@RequestBody String firebaseUrl, @PathVariable("tweet") String tweet) {
        try {
            return TwitterConfig.createImageTweet(firebaseUrl, tweet);
        }
        catch (TwitterException e) {
            return "failed";
        }
    }


}
