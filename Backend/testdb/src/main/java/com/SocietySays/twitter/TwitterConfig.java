package com.SocietySays.twitter;


import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;

public class TwitterConfig {

    public static Twitter setTwitter() {

        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey("pNPmYpwamUcjUat57umzTiXTP")
                .setOAuthConsumerSecret("LbHER5wvyzMXRLdOnfe9uMUjXolmyDtvL8PKEunaKLKIjzlIIx")
                .setOAuthAccessToken("1087113985722368002-VYlkR5saw9DcrW53fxmTby32b3kBki")
                .setOAuthAccessTokenSecret("TQCUeX6T1J3a3zzTdPutxuiac9BvP6ikTQAHhRyRysP3e");
        TwitterFactory tf = new TwitterFactory(cb.build());
        Twitter twitter = tf.getInstance();
        return twitter;

    }

    public static String createTweet(String tweet) throws TwitterException {
        try {
            Twitter twitter = setTwitter();
            Status status = twitter.updateStatus(tweet);
            return "posted";

        } catch (TwitterException e) {

            return "Failed";
        }
    }

    public static String createImageTweet(String firebaseURL, String tweet) throws TwitterException {

        URL url = null;
        try {
            url = new URL(firebaseURL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        File file = new File("/tmp/temp.jpg");

        PrintWriter writer = null;
        try {
            writer = new PrintWriter(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        writer.print("");
        writer.close();

        BufferedImage image = null;
        try {

            // read the url
            image = ImageIO.read(url);

            // for jpg
            ImageIO.write(image, "jpg", file);

        } catch (IOException e) {
            e.printStackTrace();
        }

        Twitter twitter = setTwitter();
        String statusMessage = tweet;
        StatusUpdate status = new StatusUpdate(statusMessage);
        status.setMedia(file); // set the image to be uploaded here.
        try {
            twitter.updateStatus(status);
        } catch (TwitterException e) {
            e.printStackTrace();
        }

        return "uploaded";
    }


}
