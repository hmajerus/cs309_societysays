package com.SocietySays.repository;

import com.SocietySays.auth.bean.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM user WHERE name = ?;",
            nativeQuery = true)
    void deleteUser(String id);

    @Query(value = "SELECT * FROM user WHERE name = ?;",
            nativeQuery = true)
    User findByName(String name);

    @Transactional
    @Modifying
    @Query(value = "UPDATE user u SET u.role = ? WHERE name = ?;",
            nativeQuery = true)
    void updateUserRole(int id, String name);

    @Query(value = "SELECT role FROM user WHERE name = ?",
            nativeQuery = true)
    int getRole(String name);

    @Query(value = "Select name FROM user", nativeQuery = true)
    List<String> getUserNames();
}
