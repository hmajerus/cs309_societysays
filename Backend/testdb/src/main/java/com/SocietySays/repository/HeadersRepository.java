package com.SocietySays.repository;

import com.SocietySays.headers.bean.Header;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface HeadersRepository extends JpaRepository<Header, Long> {

@Transactional
@Modifying
@Query(value = "UPDATE headers u SET u.response = ? WHERE u.id = ?;",
        nativeQuery = true)
void appendChar(String text, Long id);

@Transactional
@Modifying
@Query(value = "UPDATE headers u SET u.status = 1 WHERE u.id = ?;",
        nativeQuery = true)
void approveHeader(Long id);

@Transactional
@Modifying
@Query(value = "DELETE FROM headers WHERE id = ?;",
    nativeQuery = true)
void deleteById(Long id);

@Transactional
@Modifying
@Query(value = "UPDATE headers u SET u.timepoint = ? WHERE u.id = ?;",
        nativeQuery = true)
void updateTimepoint(String timepoint, Long id);

@Query(value = "SELECT * FROM headers where status = ?;",
        nativeQuery = true)
List<Header> getByStatus(int status);

@Transactional
@Modifying
@Query(value = "UPDATE headers u SET u.status = 2 WHERE u.id = ?;",
        nativeQuery = true)
void setActive(Long id);
}