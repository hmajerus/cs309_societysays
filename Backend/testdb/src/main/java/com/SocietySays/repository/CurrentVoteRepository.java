package com.SocietySays.repository;

import com.SocietySays.votes.bean.CurrentVote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface CurrentVoteRepository extends JpaRepository<CurrentVote, Long> {

@Transactional
@Modifying
@Query(value = "UPDATE current_vote u SET u.votes = ? WHERE u.id = ?;",
		nativeQuery = true)
void incrementVote(Integer votes, Long id);

@Transactional
@Modifying
@Query(value = "TRUNCATE TABLE current_vote;", nativeQuery = true)
void clearTable();

}