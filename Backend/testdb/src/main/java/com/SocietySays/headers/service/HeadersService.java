package com.SocietySays.headers.service;

import com.SocietySays.headers.bean.Header;
import com.SocietySays.repository.HeadersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class HeadersService implements IHeadersService {

	@Autowired
	private HeadersRepository repository;

	@Override
	public List<Header> findAll() {
		return repository.findAll();
	}

	public Header findById(Long id) {
		return repository.findOne(id);
	}

	@Transactional
	public String deleteHeader(Long id) {
	    // Header header = repository.getOne(id);
		repository.deleteById(id);
	    return "deleted";
    }

	@Transactional
	public Header create(Header header) {
		return repository.save(header);
	}

    @Transactional
    public Header appendChar(char nextChar, Long id) {
        String newText;
        Header header = repository.findOne(id);
        String response;
        if (nextChar != '×') {
        	response = header.getResponse().concat(String.valueOf(nextChar));
        	newText = repository.findOne(id).getHeaderText().concat(String.valueOf(nextChar));
		}
        else {
        	response = header.getResponse().substring(0, header.getResponse().length() - 1);
        	newText  = header.getResponse().substring(0, header.getResponse().length() - 1);
		}
        header.setResponse(response);
        repository.appendChar(newText, id);
        return header;
    }

    @Transactional
    public Header approveHeader(Long id) {
	    repository.approveHeader(id);
	    return repository.findOne(id);
    }

    public List<Header> getByStatus(int status) {
		return repository.getByStatus(status);
	}
}