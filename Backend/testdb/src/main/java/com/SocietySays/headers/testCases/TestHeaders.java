package com.SocietySays.headers.testCases;

import com.SocietySays.headers.bean.Header;
import com.SocietySays.headers.service.HeadersService;
import com.SocietySays.repository.HeadersRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class TestHeaders {

    @InjectMocks
    HeadersService headersService;

    @Mock
    HeadersRepository repository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getByStatusTest() {
        Header one = new Header(0, "hello there my name is jeff", null, "bep_bep1");
        Header two = new Header(0, "hello jeff how are you?", null, "bep_bep2");
        Header three = new Header(0, "I'm good thanks", null, "bep_bep1");
        one.setStatus(1);
        two.setStatus(1);
        three.setStatus(1);
        List<Header> headers = new ArrayList<Header>();
        headers.add(one);
        headers.add(two);
        headers.add(three);

        when(repository.getByStatus(1)).thenReturn(headers);

        List<Header> headersNew = headersService.getByStatus(1);

        assertEquals(3, headers.size());
        verify(repository, times(1)).getByStatus(1);
    }

    @Test
    public void findOneTest() {

        Header header = new Header(0, "hello there my name is jeff", null, "bep_bep1");

        when(headersService.findById(header.getId())).thenReturn(header);

        assertEquals("hello there my name is jeff", header.getHeaderText());
        assertEquals(0, header.getIsImg());
        assertNull(header.getImgFile());
        assertEquals("bep_bep1", header.getUploader());
        assertEquals("", header.getResponse());
    }

    @Test
    public void findAllTest() {

        // Header header = new Header(0, "hello there my name is jeff", null, "bep_bep1"");
        Header one = new Header(0, "hello there my name is jeff", null, "bep_bep1");
        Header two = new Header(0, "hello jeff how are you?", null, "bep_bep2");
        Header three = new Header(0, "I'm good thanks", null, "bep_bep1");

        List<Header> headers = new ArrayList<Header>();
        headers.add(one);
        headers.add(two);
        headers.add(three);

        when(repository.findAll()).thenReturn(headers);

        repository.findAll();

        assertEquals(3, headers.size());
        verify(repository, times(1)).findAll();
    }
}