package com.SocietySays.headers.bean;

import javax.persistence.*;

@Entity
@Table(name = "headers")
public class Header {

	@Id
	@GeneratedValue
	private Long id;
	
	@Column(name = "is_img")
	private int isImg;
	
	private int status;
	
	@Column(name = "header_text")
	private String headerText;
	
	@Column(name = "img_file")
	private String imgFile;
	
	private String uploader;
	
	private String response;

	private String timepoint;
	
	public Header() {
	}

	/**
	 * Constructs a Header object
	 * @param isImg
	 * @param text
	 * @param imgFile
	 * @param uploader
	 */
	public Header(int isImg, String text, String imgFile, String uploader) {
		super();
		this.isImg = isImg;
		this.status = 0;
		this.headerText = text;
		this.imgFile = imgFile;
		this.uploader = uploader;
		this.response = "";
	}


	/**
	 * Get the response string of this Header
	 * @return response
	 */
	public String getResponse() {
		return response;
	}

	/**
	 * Set the response string of this Header
	 * @param response
	 */
	public void setResponse(String response) {
		this.response = response;
	}

	/**
	 * Get the timepoint of this Header
	 * @return timepoint
	 */
	public String getTimepoint() {
		return timepoint;
	}

	/**
	 * Set the timepoint of this Header
	 * @param timepoint
	 */
	public void setTimepoint(String timepoint) {
		this.timepoint = timepoint;
	}

	/**
	 * Get the id of this Header
	 * @return id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Get the isImg boolean of this header
	 * @return isImg
	 */
	public int getIsImg() {
		return isImg;
	}

	/**
	 * Set the isImg boolean of this Header
	 * @param isImg
	 */
	public void setisImg(int isImg) {
		this.isImg = isImg;
	}

	/**
	 * Get the status of this Header
	 * @return status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * Set the status of this Header
	 * @param status
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * Get the headerText of this Header
	 * @return headerText
	 */
	public String getHeaderText() {
		return headerText;
	}

	/**
	 * Set the headerText of this Header
	 * @param text
	 */
	public void setHeaderText(String text) {
		this.headerText = text;
	}

	/**
	 * Get the Firebase image filename of this header
	 * @return imgFile
	 */
	public String getImgFile() {
		return imgFile;
	}

	/**
	 * Set the Firebase image filename of this Header
	 * @param imgFile
	 */
	public void setImgFile(String imgFile) {
		this.imgFile = imgFile;
	}

	/**
	 * Get the username of the user that uploaded this Header
	 * @return uploader
	 */
	public String getUploader() {
		return uploader;
	}

	/**
	 * Set the username field of this Header
	 * @param uploader
	 */
	public void setUploader(String uploader) {
		this.uploader = uploader;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Header other = (Header) obj;
		if (status != other.status)
			return false;
		if (imgFile == null) {
			if (other.imgFile != null)
				return false;
		} else if (!imgFile.equals(other.imgFile))
			return false;
		if (isImg != other.isImg)
			return false;
		if (headerText == null) {
			if (other.headerText != null)
				return false;
		} else if (!headerText.equals(other.headerText))
			return false;
		if (uploader == null) {
			if (other.uploader != null)
				return false;
		} else if (!uploader.equals(other.uploader))
			return false;
		return true;
	}
}
