package com.SocietySays.headers.controller;

import com.SocietySays.headers.bean.Header;
import com.SocietySays.headers.service.IHeadersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class HeadersController {

	@Autowired
	private IHeadersService headerService;

	/**
	 * Return a list of all Headers in the table
	 * @return List of Headers
	 */
	@GetMapping("/showHeaders")
	public List<Header> findHeaders() {
		return headerService.findAll();
	}

	/**
	 * Show a single Header by its ID
	 * @param id
	 * @return A Header object
	 */
	@GetMapping("/showHeaders/{id}")
	public Header findById(@PathVariable("id") Long id) {
		return headerService.findById(id);
	}

	/**
	 * Send a Header object to the database
	 * @param header
	 * @return The Header object that was sent
	 */
	@PostMapping("/sendHeader")
	public Header sendHeader(@RequestBody Header header) {
		return headerService.create(header);
	}

	/**
	 * Append a char to the response string of a Header
	 * @param id
	 * @param nextChar
	 * @return The Header object with an updated response string
	 */
	@PostMapping("/appendChar")
	public Header appendChar(@RequestBody Long id, @RequestBody char nextChar) {
		return headerService.appendChar(nextChar, id);
	}

	/**
	 * Set the status of a Header to 1 (approved)
	 * @param header
	 * @return Newly approved Header
	 */
	@PostMapping("/approveHeader")
	public Header approveHeader(@RequestBody Header header) {
		return headerService.approveHeader(header.getId());
	}

	/**
	 * Delete a Header by ID
	 * @param id
	 * @return String "deleted"
	 */
	@GetMapping("/deleteHeader/{id}")
	public String deleteHeader(@PathVariable String id) {
		return headerService.deleteHeader((long) Integer.parseInt(id));
	}

	/**
	 * Return all Headers with a given status
	 * @param status
	 * @return List of Headers
	 */
	@GetMapping("/getByStatus")
	public List<Header> getByStatus(String status) {
		return headerService.getByStatus(Integer.parseInt(status));
	}
}
