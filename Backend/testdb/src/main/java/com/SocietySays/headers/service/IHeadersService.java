package com.SocietySays.headers.service;

import com.SocietySays.headers.bean.Header;

import java.util.List;

public interface IHeadersService {

	List<Header> findAll();
	
	Header findById(Long id);
	
	Header create(Header header);

	Header appendChar(char nextChar, Long id);

	Header approveHeader(Long id);

	String deleteHeader(Long id);

	List<Header> getByStatus(int status);
}
