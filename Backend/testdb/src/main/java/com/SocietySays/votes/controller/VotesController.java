/**
 * VotesController communicates with the MySQL database to
 * display and configure the votes table
 */
package com.SocietySays.votes.controller;

import com.SocietySays.votes.bean.CurrentVote;
import com.SocietySays.votes.service.ICurrentVoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class VotesController {

    @Autowired
    private ICurrentVoteService voteService;

    /**
     * findVotes returns a list of all the current votes
     * @return
     */
    @GetMapping("/showVotes")
    public List<CurrentVote> findVotes() {
    	return voteService.findAll();
    }

    /**
     * vote sends the user submitted vote to the database
     * @param vote
     * @return a vote object
     */
    @PostMapping("/sendVote")
    CurrentVote vote(@RequestBody CurrentVote vote) {
        return voteService.create(vote);
    }

    /**
     * deleteAll truncates the current vote mysql table
     */
    @DeleteMapping("/truncateVotesTable")
    public void deleteAll() {voteService.deleteAll();}
}