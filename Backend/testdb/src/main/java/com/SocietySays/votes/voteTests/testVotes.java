package com.SocietySays.votes.voteTests;

import com.SocietySays.repository.CurrentVoteRepository;
import com.SocietySays.votes.bean.CurrentVote;
import com.SocietySays.votes.service.CurrentVoteService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class testVotes {

    @InjectMocks
    CurrentVoteService voteService;

    @Mock
    CurrentVoteRepository repository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getVoteIDTest() {

        CurrentVote vote1 = new CurrentVote(1, 'a', 10);
        CurrentVote vote2 = new CurrentVote(2, 'b', 20);
        CurrentVote vote2Confirm;

        List<CurrentVote> votes = new ArrayList<>();
        votes.add(vote1);
        votes.add(vote2);

        when(repository.getOne((long) 2)).thenReturn(vote2);

        vote2Confirm = repository.getOne((long) 2);

        assertEquals(vote2, vote2Confirm);
        verify(repository, times(1)).getOne((long) 2);

    }

    @Test
    public void findAllTest() {
        CurrentVote vote1 = new CurrentVote(1, 'a', 10);
        CurrentVote vote2 = new CurrentVote(2, 'b', 20);
        CurrentVote vote3 = new CurrentVote(3, 'c', 30);
        CurrentVote vote4 = new CurrentVote(4, 'd', 40);


        List<CurrentVote> votes = new ArrayList<>();
        votes.add(vote1);
        votes.add(vote2);
        votes.add(vote3);
        votes.add(vote4);

        when(repository.findAll()).thenReturn(votes);

        repository.findAll();

        assertEquals(4, votes.size());

        verify(repository, times(1)).findAll();

    }

    @Test
    public void deleteAllVoteTest() {

        CurrentVote vote1 = new CurrentVote(1, 'a', 10);
        CurrentVote vote2 = new CurrentVote(2, 'b', 20);
        CurrentVote vote3 = new CurrentVote(3, 'c', 30);
        CurrentVote vote4 = new CurrentVote(4, 'd', 40);


        List<CurrentVote> votes = new ArrayList<>();
        votes.add(vote1);
        votes.add(vote2);
        votes.add(vote3);
        votes.add(vote4);

        doNothing().when(repository).deleteAll();

        repository.deleteAll();
        assertEquals(repository.findAll().size(), 0);


    }
}
