package com.SocietySays.votes.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "current_vote")
public class CurrentVote {

	@Id
	@GeneratedValue
    private Long id;

    private int tID;

    private char letter;
    
    private int votes;

    public CurrentVote() {
    }

    /**
     * Create a CurrentVote object
     * @param tID
     * @param letter
     * @param votes
     */
    public CurrentVote(int tID, char letter, int votes) {
        this.tID = tID;
        this.letter = letter;
        this.votes = votes;
    }

    /**
     * Get the ID of this CurrentVote object
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * Set the ID of this CurrentVote object
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Get the letter of this CurrentVote object
     * @return
     */
    public char getLetter() {
        return letter;
    }

    /**
     * Set the letter of this CurrentVote object
     * @param letter
     */
    public void setLetter(char letter) {
        this.letter = letter;
    }

    /**
     * Get the number of votes of this CurrentVote object
     * @return
     */
    public int getVotes() {
        return votes;
    }

    /**
     * Set the number of votes of this CurrentVote object
     * @param votes
     */
    public void setVotes(int votes) {
        this.votes = votes;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CurrentVote other = (CurrentVote) obj;
        if (this.votes != other.votes) {
            return false;
        }
        if (!Objects.equals(this.letter, other.letter)) {
            return false;
        }
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "Votes{" + "id=" + id + ", tid=" + tID + ", char=" + letter + ", Vote Count=" + votes + ' ' + '}';
    }
    
    public boolean compareVotes(CurrentVote vote1, CurrentVote vote2) {
    	return (vote1.getVotes()>vote2.getVotes());
    }
}