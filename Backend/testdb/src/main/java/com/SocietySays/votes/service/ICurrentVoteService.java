package com.SocietySays.votes.service;

import com.SocietySays.votes.bean.CurrentVote;

import java.util.List;

public interface ICurrentVoteService {

    List<CurrentVote> findAll();
    
    CurrentVote create(CurrentVote vote);

    Void deleteAll();
    
}