package com.SocietySays.auth.testCases;

import com.SocietySays.auth.bean.User;
import com.SocietySays.auth.controller.LoginController;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
//import com.mockito.account.Account;
//import com.mockito.account.AccountRepository;
//import com.mockito.account.AccountService;

public class testUserLogins {

    @InjectMocks
    LoginController acctService;

    @Mock
    LoginController repo;

    @Mock
    User testUser = new User();
    User testUser2 = new User();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateUser() {

        when(acctService.create(testUser)).thenReturn(testUser);

        testUser.setEmail("test@example.com");
        testUser.setName("John Doe");
        testUser.setPassword("password");
        testUser.setId(999);
        testUser.setRole(1);



        assertEquals(testUser, testUser2);

    }

    @Test
    public void testFindUser() {

        testUser.setEmail("test2@example.com");
        testUser.setName("John Dole");
        testUser.setPassword("password");
        testUser.setId(19999);
        testUser.setRole(1);

        when(acctService.getUser(testUser.getName())).thenReturn(testUser2);
        assertEquals(testUser.getName(),testUser2.getName());
    }

    @Test
    public void testDeleteUser() {

        testUser.setEmail("test3@example.com");
        testUser.setName("John Doles");
        testUser.setPassword("password");
        testUser.setId(20120);
        testUser.setRole(1);

        when(acctService.deleteUser(testUser.getName())).thenReturn("deleted");
        assertEquals(acctService.getUser(testUser.getEmail()), "");

    }




}
