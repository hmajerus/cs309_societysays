/**
 * The User class stores and creates the user object and bean for
 * login authentication. It also contains the getters and setters
 * for setting and retrieving login information.
 */



package com.SocietySays.auth.bean;

import javax.persistence.*;

@Entity // This tells Hibernate to make a table out of this class
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

    private String name;

    private String email;

    private String password;

    private Integer role;

//    public User() {
//
//    }
//
//    public User(String name, String email, String password, Integer role) {
//        this.name = name;
//        this.email = email;
//        this.password = password;
//        this.role = role;
//    }

    /**
     * Returns the database id
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the database id
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Gets a username
     * @return username
     */
    public String getName() {
        return name;
    }


    /**
     * Sets a username
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets an email
     * @return email address
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets an email address
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets the user's password
     * @return password
     */
    public String getPassword() {
        return password;
    }


    /**
     * Sets the user's password
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets the user's role
     * (User, Moderator, Admin)
     * @return role
     */
    public Integer getRole() {
        return role;
    }

    /**
     * Sets the user's role
     * (User, Moderator, Admin)
     * @param role
     */
    public void setRole(Integer role) {
        this.role = role;
    }
}
