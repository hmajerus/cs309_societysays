package com.example.volleytest;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.volleytest.app.AppController;

import com.example.volleytest.net_utils.Const;
import com.example.volleytest.net_utils.LruBitmapCache;
public class StringRequestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_string_request);
    }

    public void stringRequest(View v){

        final TextView mTextView = (TextView) findViewById(R.id.textView2);

        RequestQueue queue = Volley.newRequestQueue(this);
        /*
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_STRING_REQ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        mTextView.setText("Response is: "+ response.substring(0,500));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mTextView.setText("That didn't work!");
            }
        });
        queue.add(stringRequest);
        */
        String tag_string_req ="string_req";
        String url ="https://api.androidhive.info/volley/string_response.html";
        /*final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();*/
        StringRequest strReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>(){
            @Override
                    public void onResponse(String response) {
                //Log.d(TAG, response.toString());
                mTextView.setText("Response is: "+ response);
            }
        },new Response.ErrorListener() {
                    @Override
                            public void onErrorResponse(VolleyError error) {
                //VolleyLog.d(TAG,"Error: "+ error.getMessage());
                        mTextView.setText("That didn't work!");
            }
        });
        //AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        queue.add(strReq);
    }
}
