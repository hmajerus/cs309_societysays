package com.example.volleytest;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.volleytest.app.AppController;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class JSONRequestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jsonrequest);
    }

    public void objRequest(View V){
        boolean isPOST = false;
        if(!isPOST) {
            final TextView mTextView = (TextView) findViewById(R.id.textView2);
            RequestQueue queue = Volley.newRequestQueue(this);
            String tag_json_obj = "json_obj_req";
            String url = "https://api.androidhive.info/volley/person_object.json";
        /*
        ProgressDialog pDialog =new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.show();
        */
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    mTextView.setText("OBJ_REQ_GET\n" + response.toString());
                    //pDialog.hide();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mTextView.setText("Whoopsie, that did go through");
                    // hide the progress dialog
                    //pDialog.hide();
                }
            });
            //AppController.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);
            queue.add(jsonObjReq);
        }
        else{
            final TextView mTextView = (TextView) findViewById(R.id.textView2);
            RequestQueue queue = Volley.newRequestQueue(this);
            String tag_json_obj ="json_obj_req";
            String url ="https://api.androidhive.info/volley/person_object.json";
            JsonObjectRequest jsonObjReq =new JsonObjectRequest(Request.Method.POST,
                    url,null,
                    new Response.Listener<JSONObject>() {
                @Override
                        public void onResponse(JSONObject response) {
                        mTextView.setText("OBJ_REQ_POST:\n"+response.toString());
                }
            },new Response.ErrorListener() {
                @Override
                        public void onErrorResponse(VolleyError error) {
                        mTextView.setText("Well Shoot this messed up:" + error.toString());
                }
            }) {
                @Override
                protected Map<String, String>getParams() {
                    Map<String, String> params =new HashMap<String, String>();

                    params.put("name","Androidhive");
                    params.put("email","abc@androidhive.info");
                    params.put("password","password123");
                    return params;
                }
            };
            queue.add(jsonObjReq);
        }
    }

    public void arrRequest(View v){
        final TextView mTextView = (TextView) findViewById(R.id.textView3);
        RequestQueue queue = Volley.newRequestQueue(this);
        String tag_json_arry ="json_array_req";
        String url ="https://api.androidhive.info/volley/person_array.json";
        JsonArrayRequest req = new JsonArrayRequest(url,
                new Response.Listener<JSONArray>() {
            @Override
                    public void onResponse(JSONArray response) {
                        mTextView.setText("ARR_REQ\n"+ response.toString());

            }
        },new Response.ErrorListener() {
            @Override
                    public void onErrorResponse(VolleyError error) {
                        mTextView.setText("UH OH");
            }
        });
        queue.add(req);
    }
}
