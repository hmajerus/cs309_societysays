package com.example.sneezecounter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.lang.Math;

import static android.text.TextUtils.indexOf;

public class MainActivity extends AppCompatActivity {
    ArrayList<Date> sneezes = new ArrayList<Date>();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        load();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    /*
    @Override
    public void onResume(){
        load();
        super.onResume();
    }
    */
    public void OnSneeze(View v){
        sneezes.add(new Date());
        TextView ss = (TextView) findViewById(R.id.sneezesStatement);
        ss.setText(getSneezesStatment());
    }

    public void earlyDate(View v){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2019);
        cal.set(Calendar.MONTH, Calendar.JANUARY);
        cal.set(Calendar.DAY_OF_MONTH, 20);
        cal.set(Calendar.HOUR, 8 );
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0 );
        Date dateRepresentation = cal.getTime();
        sneezes.add(dateRepresentation);
    }

    public String getSneezesStatment(){
        long diff =  sneezes.get(sneezes.size()-1).getTime()-sneezes.get(0).getTime();
        long daysTotal = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
        double unRound = ((double)sneezes.size()) / ((int)daysTotal);
        //double sPerD = Math.round(unRound*100.0) / 100.0;
        return "You have sneezed " + sneezes.size() + " times in the last " + daysTotal + " days. This gives you an average of " + String.format("%.2f", unRound) + " sneezes per day.";
    }

    @Override
    public void onStop() {
        save();
        System.out.println("onStop ran");
        super.onStop();
    }

    public String getCSV(){
        String csv = "";
        int bigness = sneezes.size();
        int curr = 0;
        while(true){
            if(curr == bigness){
                break;
            }
            //csv += DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).format(sneezes.get(curr));
            csv += new SimpleDateFormat("MM-dd-yyyy HH:mm:ss").format(sneezes.get(curr)) + ",";
            curr++;
        }
        return csv;
    }
    public void save(){
        String filename = "sneezes.txt";
        String fileContents = getCSV();
        FileOutputStream outputStream;

        try {
            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(fileContents.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void load() {
        BufferedReader br;
        StringBuilder sb;
        String fileAsString;
        try {
            br = new BufferedReader(new FileReader("sneezes.txt"));
            sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line).append("\n");
                line = br.readLine();
            }
            fileAsString = sb.toString();
        }catch(IOException e) {

        }


    }
}
