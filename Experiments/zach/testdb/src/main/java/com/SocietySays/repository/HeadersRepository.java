package com.SocietySays.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.SocietySays.headers.bean.Header;

@Repository
public interface HeadersRepository extends JpaRepository<Header, Long> {

}
