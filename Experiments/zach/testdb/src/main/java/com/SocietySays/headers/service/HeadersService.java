package com.SocietySays.headers.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.SocietySays.headers.bean.Header;
import com.SocietySays.repository.HeadersRepository;

@Service
public class HeadersService implements IHeadersService {
	
	@Autowired
	private HeadersRepository repository;
	
	@Override
	public List<Header> findAll() {
		List<Header> headers = repository.findAll();
		return headers;
	}
	
	public Header findById(Long id) {
		return repository.findOne(id);
	}
	
	@Transactional
	public Header create(Header header) {
		return repository.save(header);
	}
}
