package com.SocietySays.votes.service;

import java.util.List;

import com.SocietySays.votes.bean.CurrentVote;

public interface ICurrentVoteService {

    public List<CurrentVote> findAll();
    
    public CurrentVote create(CurrentVote vote);
    
}