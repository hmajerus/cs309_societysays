package com.SocietySays.headers.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "headers")
public class Header {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private int isImg;
	
	private int approved;
	
	private String text;
	
	private String imgFile;
	
	private String uploader;
	
	public Header() {
	}
	
	public Header(int isImg, String text, String imgFile, String uploader) {
		super();
		this.isImg = isImg;
		this.approved = 0;
		this.text = text;
		this.imgFile = imgFile;
		this.uploader = uploader;
	}

	public Long getId() {
		return id;
	}

	public int getIsImg() {
		return isImg;
	}

	public void setImgText(int isImg) {
		this.isImg = isImg;
	}

	public int getApproved() {
		return approved;
	}

	public void setApproved(int approved) {
		this.approved = approved;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public String getImgFile() {
		return imgFile;
	}

	public void setImgFile(String imgFile) {
		this.imgFile = imgFile;
	}

	public String getUploader() {
		return uploader;
	}

	public void setUploader(String uploader) {
		this.uploader = uploader;
	}

//	@Override
//	public int hashCode() {
//		final int prime = 31;
//		int result = 1;
//		result = prime * result + (approved ? 1231 : 1237);
//		result = prime * result + ((imgFile == null) ? 0 : imgFile.hashCode());
//		result = prime * result + (imgText ? 1231 : 1237);
//		result = prime * result + ((text == null) ? 0 : text.hashCode());
//		result = prime * result + ((uploader == null) ? 0 : uploader.hashCode());
//		return result;
//	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Header other = (Header) obj;
		if (approved != other.approved)
			return false;
		if (imgFile == null) {
			if (other.imgFile != null)
				return false;
		} else if (!imgFile.equals(other.imgFile))
			return false;
		if (isImg != other.isImg)
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		if (uploader == null) {
			if (other.uploader != null)
				return false;
		} else if (!uploader.equals(other.uploader))
			return false;
		return true;
	}
	
}
