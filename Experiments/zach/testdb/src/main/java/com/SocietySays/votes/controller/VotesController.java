package com.SocietySays.votes.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.SocietySays.votes.bean.CurrentVote;
import com.SocietySays.votes.service.ICurrentVoteService;

@RestController
public class VotesController {
	
    @Autowired
    ICurrentVoteService voteService;
    
    @GetMapping("/showVotes")
    public List<CurrentVote> findVotes() {
    	List<CurrentVote> votes = voteService.findAll();
    	return votes;
    }
    
    @PostMapping("/sendVote")
    CurrentVote vote(@RequestBody CurrentVote vote) {	
    	return voteService.create(vote);
    }
}