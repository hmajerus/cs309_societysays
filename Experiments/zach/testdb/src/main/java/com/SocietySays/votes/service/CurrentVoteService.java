package com.SocietySays.votes.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.SocietySays.repository.CurrentVoteRepository;
import com.SocietySays.votes.bean.CurrentVote;

@Service
public class CurrentVoteService implements ICurrentVoteService {

    @Autowired
    private CurrentVoteRepository repository;

    @Override
    public List<CurrentVote> findAll() {

        List<CurrentVote> votes = repository.findAll(new Sort(Direction.DESC, "votes"));

        return votes;
    }
    
    @Transactional
    public CurrentVote create(CurrentVote vote) {
    	List<CurrentVote> votes = repository.findAll();
    	for (CurrentVote i :  votes) {
    		if (i.getLetter() == vote.getLetter()) {
    			vote = i;
    			vote.setVotes(vote.getVotes()+1);
    			repository.incrementVote(vote.getVotes(), vote.getId());
    			return vote;
    		}
    	}
    	return repository.save(vote);
    }
}