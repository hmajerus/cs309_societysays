package com.SocietySays.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.SocietySays.votes.bean.CurrentVote;

@Repository
public interface CurrentVoteRepository extends JpaRepository<CurrentVote, Long> {

@Modifying
@Query(value = "UPDATE current_vote u SET u.votes = ? WHERE u.id = ?;",
		nativeQuery = true)
void incrementVote(Integer votes, Long id);
}