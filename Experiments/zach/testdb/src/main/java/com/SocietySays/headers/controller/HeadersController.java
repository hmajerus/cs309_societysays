package com.SocietySays.headers.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.SocietySays.headers.bean.Header;
import com.SocietySays.headers.service.IHeadersService;

@RestController
public class HeadersController {

	@Autowired
	IHeadersService headerService;
	
	@GetMapping("/showHeaders")
	public List<Header> findHeaders() {
		List<Header> headers = headerService.findAll();
		return headers;
	}
	
	@GetMapping("showHeaders/{id}")
	public Header findById(@PathVariable("id") Long id) {
		return headerService.findById(id);
	}
	
	@PostMapping("/sendHeader")
	public Header sendHeader(@RequestBody Header header) {
		
		return headerService.create(header);
	}
}
