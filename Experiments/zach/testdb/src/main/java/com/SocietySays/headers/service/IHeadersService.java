package com.SocietySays.headers.service;

import java.util.List;

import com.SocietySays.headers.bean.Header;

public interface IHeadersService {

	public List<Header> findAll();
	
	public Header findById(Long id);
	
	public Header create(Header header);
}
