package com.coms309.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestMySqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestMySqlApplication.class, args);
	}

}

