package com.example.mysqltest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class MySqlTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MySqlTestApplication.class, args);
	}

}

