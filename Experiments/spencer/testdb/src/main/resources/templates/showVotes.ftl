<!DOCTYPE html>
<html>
<head>
    <title>Votes</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<h2>List of votes</h2>

<table>
    <tr>
        <th>tID</th>
        <th>Char</th>
        <th>Vote Count</th>
    </tr>

    <#list votes as currentVote>
        <tr>
            <td>${currentVote.tID}</td>
            <td>${currentVote.letter}</td>
            <td>${currentVote.votes}</td>
        </tr>
    </#list>
</table>
</body>
</html>