package com.SocietySays.controller;

import com.SocietySays.bean.currentVote;
import com.SocietySays.repository.currentVoteRepository;
import com.SocietySays.service.IcurrentVoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CurrentVoteController {

    @Autowired
    IcurrentVoteService voteService;
    
    @Autowired
    private currentVoteRepository voteRepo;
    
    
    @GetMapping("/showVotes")
    public List<currentVote> findVotes() {

    	List<currentVote> votes = voteService.findAll();
//    	currentVote vote = new currentVote();
//    	vote.settID(1);
//    	vote.setLetter('A');
//    	vote.setVotes(0);
    	return votes;
    }
    
    
    @GetMapping(path="/sendVote") // Map ONLY GET Requests
	public @ResponseBody String sendVote (@RequestParam int id
			, @RequestParam char letter) {
		// @ResponseBody means the returned String is the response, not a view name
		// @RequestParam means it is a parameter from the GET or POST request

		currentVote n = new currentVote();
		n.setLetter(letter);
		n.settID(id);
		voteRepo.save(n);
		return "Vote received";
	}
}