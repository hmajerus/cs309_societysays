package com.SocietySays.service;

import com.SocietySays.bean.currentVote;
import com.SocietySays.repository.currentVoteRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class currentVoteService implements IcurrentVoteService {

    @Autowired
    private currentVoteRepository repository;

    @Override
    public List<currentVote> findAll() {

        List<currentVote> votes = (List<currentVote>) repository.findAll();

        return votes;
    }
}