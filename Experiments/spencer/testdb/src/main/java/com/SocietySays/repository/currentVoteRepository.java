package com.SocietySays.repository;

import com.SocietySays.bean.currentVote;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface currentVoteRepository extends JpaRepository<currentVote, Long> {

}