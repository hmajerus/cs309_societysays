package com.SocietySays.bean;

import java.util.Objects;
import javax.persistence.*;

@Entity
@Table(name = "current_vote")
public class currentVote {

    @Id

    private Long id;
    private int tID;

    private char letter;
    private int votes;

    public currentVote() {
    }

    public currentVote(Long id, int tID, char letter, int votes) {
        this.id = id;
        this.tID = tID;
        this.letter = letter;
        this.votes = votes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int gettID() {
        return tID;
    }

    public void settID(int tID) {
        this.tID = tID;
    }

    public char getLetter() {
        return letter;
    }

    public void setLetter(char letter) {
        this.letter = letter;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.id);
        hash = 79 * hash + Objects.hashCode(this.tID);
        hash = 79 * hash + Objects.hashCode(this.letter);
        hash = 79 * hash + this.votes;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final currentVote other = (currentVote) obj;
        if (this.votes != other.votes) {
            return false;
        }
        if (!Objects.equals(this.letter, other.letter)) {
            return false;
        }
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "Votes{" + "id=" + id + ", tid=" + tID + ", char=" + letter + ", Vote Count=" + votes + ' ' + '}';
    }
}