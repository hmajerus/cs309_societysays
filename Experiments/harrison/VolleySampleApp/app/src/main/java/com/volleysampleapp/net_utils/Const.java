package com.volleysampleapp.net_utils;

public class Const {
    public static final String URL_JSON_OBJECT = "https://api.androidhive.info/volley/person_object.json";
    public static final String URL_JSON_ARRAY = "https://api.androidhive.info/volley/person_array.json";
    public static final String URL_STRING_REQ = "https://api.androidhive.info/volley/atrig_response.html";
    //public static final String URL_IMAGE = "https://api.androidhive.info/volley/volley-image.jpg";
    public static final String URL_IMAGE = "https://s3.amazonaws.com/cdn-origin-etr.akc.org/wp-content/uploads/2017/11/12193133/German-Shepherd-Puppy-Fetch.jpg";

}
