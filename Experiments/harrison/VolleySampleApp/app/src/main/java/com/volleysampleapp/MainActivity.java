package com.volleysampleapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void toStringReqActivity(View v){
        Intent startNewActivity = new Intent(this, StringRequestActivity.class);
        startActivity(startNewActivity);
    }

    public void toJsonReqActivity(View v){
        Intent startNewActivity = new Intent(this, JSONRequestActivity.class);
        startActivity(startNewActivity);
    }

    public void toImageReqActivity(View v){
        Intent startNewActivity = new Intent(this, ImageRequestActivity.class);
        startActivity(startNewActivity);
    }
}
