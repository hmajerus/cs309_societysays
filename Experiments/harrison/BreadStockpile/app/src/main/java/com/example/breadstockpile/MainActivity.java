package com.example.breadstockpile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                TextView textR = (TextView)findViewById(R.id.textView3);
                int remaining = getNumber(textR) - 1;
                if(remaining > -1) {
                    TextView textB = (TextView) findViewById(R.id.textView2);
                    int num = getNumber(textB) + 1;
                    textB.setText("Bread Count : " + num);

                    textR.setText("Remaining Storage : " + remaining);
                } else{
                    Toast newToast = Toast.makeText(getApplicationContext(), "You need to upgrade your storage!", Toast.LENGTH_SHORT);
                    newToast.show();
                }
            }
        });

        final Button button2 = findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                TextView textB = (TextView)findViewById(R.id.textView2);
                int num = getNumber(textB) - 1;
                if(num == -1){
                    Toast newToast = Toast.makeText(getApplicationContext(), "You have no bread left!", Toast.LENGTH_SHORT);
                    newToast.show();
                } else{
                    textB.setText("Bread Count : " + num);

                    TextView textR = (TextView)findViewById(R.id.textView3);
                    int remaining = getNumber(textR) + 1;
                    textR.setText("Remaining Storage : " + remaining);
                }
            }
        });

        final Button button3 = findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                TextView textR = (TextView)findViewById(R.id.textView3);
                int remaining = getNumber(textR) + 5;
                textR.setText("Remaining Storage : " + remaining);

                TextView textT = (TextView)findViewById(R.id.textView4);
                int total = getNumber(textT) + 5;
                textT.setText("Total Storage : " + total);
            }
        });
}

    public int getNumber(TextView text){
        CharSequence chars = text.getText();
        String charStr = chars.toString();
        int strt;
        if(charStr.charAt(0) == 'B'){
            strt = 14;
        } else if (charStr.charAt(0) == 'R') {
            strt = 20;
        } else{
            strt = 16;
        }
        String numbers = "";
        for(int i = strt; i < charStr.length(); i++){
            numbers = numbers + charStr.charAt(i) + "";
        }
        int num = Integer.parseInt(numbers);
        return num;
    }

    public void viewBread(View view){
        Intent startNewActivity = new Intent(this, BreadViewer.class);
        startActivity(startNewActivity);
    }

    public void goToUITesting(View view){
        Intent startNewActivity = new Intent(this, UITestingActivity.class);
        startActivity(startNewActivity);
    }
}
